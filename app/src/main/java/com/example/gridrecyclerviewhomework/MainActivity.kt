package com.example.gridrecyclerviewhomework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val musiciansList = mutableListOf<MusicianModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        adapter = RecyclerViewAdapter(musiciansList)
        recyclerView.layoutManager = GridLayoutManager(this, 3)

        recyclerView.adapter = adapter
        setData()

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                setData()
                adapter.notifyDataSetChanged()
            }, 2000)
        }
    }

    private fun refresh() {
        musiciansList.clear()
        adapter.notifyDataSetChanged()
    }

    private fun setData() {

        musiciansList.add(
            0, MusicianModel("Queen", "rock")
        )
        musiciansList.add(
            0, MusicianModel("Worakls", "techno")
        )
        musiciansList.add(
            0, MusicianModel("Pink Floyd", "rock")
        )
        musiciansList.add(
            0, MusicianModel("Joachim Pastor", "techno")
        )
        musiciansList.add(
            0, MusicianModel("Dimmu Borgir", "rock")
        )
        musiciansList.add(
            0, MusicianModel("Burzum", "rock")
        )
        musiciansList.add(
            0, MusicianModel("N'to", "techno")
        )
        musiciansList.add(
            0, MusicianModel("Mathame", "techno")
        )
        musiciansList.add(
            0, MusicianModel("Arctic Monkeys", "rock")
        )
        musiciansList.add(
            0, MusicianModel("Behemoth", "rock")
        )
        musiciansList.add(
            0, MusicianModel("Queen", "rock")
        )
        musiciansList.add(
            0, MusicianModel("Worakls", "techno")
        )
        musiciansList.add(
            0, MusicianModel("Pink Floyd", "rock")
        )
        musiciansList.add(
            0, MusicianModel("Joachim Pastor", "techno")
        )
        musiciansList.add(
            0, MusicianModel("Dimmu Borgir", "rock")
        )
        musiciansList.add(
            0, MusicianModel("Burzum", "rock")
        )
        musiciansList.add(
            0, MusicianModel("N'to", "techno")
        )
        musiciansList.add(
            0, MusicianModel("Mathame", "techno")
        )
        musiciansList.add(
            0, MusicianModel("Arctic Monkeys", "rock")
        )
        musiciansList.add(
            0, MusicianModel("Behemoth", "rock")
        )



    }

}