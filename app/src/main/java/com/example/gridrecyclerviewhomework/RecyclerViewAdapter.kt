package com.example.gridrecyclerviewhomework

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.rock_musician_layout.view.*
import kotlinx.android.synthetic.main.techno_musician_layout.view.*

class RecyclerViewAdapter(
    private val musicians: MutableList<MusicianModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TECHNO_MUSICIAN = 1
        const val ROCK_MUSICIAN = 2
    }


    inner class TechnoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {
            val model = musicians[adapterPosition]
            itemView.technoMusicianTextView.text = model.name
            itemView.technoImageView.setImageResource(R.mipmap.techno)

        }
    }

    inner class RockViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val model = musicians[adapterPosition]
            itemView.rockMusicianTextView.text = model.name
            itemView.rockImageView.setImageResource(R.mipmap.rock)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TECHNO_MUSICIAN) {
            TechnoViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.techno_musician_layout,
                    parent,
                    false
                )
            )
        } else {
            RockViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.rock_musician_layout,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount() = musicians.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is TechnoViewHolder)
            holder.onBind()
        else if (holder is RockViewHolder)
            holder.onBind()

    }

    override fun getItemViewType(position: Int): Int {
        val model = musicians[position]
        return if (model.genre == "techno") {
            TECHNO_MUSICIAN
        } else {
            ROCK_MUSICIAN
        }

    }
}